Welcome to Backend Engineer App<br>

- Firstly clone this repo to set up your localhost for RN fetches
  - `npm install`
  - `npm run server` for GraphQL server (Preferred)
  - `npm run json-server` for REST API

Graphql server will open graphiql playground in `http://localhost:4000/`.

> <br>

REST API will be served in `http://localhost:5000`.
