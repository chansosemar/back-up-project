import React from 'react';
import {Text, View, Image, SafeAreaView,StyleSheet, Button,ScrollView} from 'react-native';
import Slider from '../components/Slider';
import Jokes from '../components/Jokes';
import Weather from '../components/Weather';
export default class Dashboard extends React.Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (
			
				<ScrollView style={styles.container}>
					<View style={styles.header}>
						<Image source={{uri:'https://i.pinimg.com/originals/0c/3b/3a/0c3b3adb1a7530892e55ef36d3be6cb8.png'}} style={{width:30,height:30,marginRight:10,}}/>
						<Text style={styles.title}>Halo Chandra Tampan dan Berani</Text>
					</View>

					<View  style={styles.content}>
						<View styles={styles.stack}>
							<Button style={styles.tombol} 
							onPress={()=> this.props.navigation.navigate('OtherPage')} title='GO TO OtherPage'>
							</Button>
							<Button style={styles.tombol}
							onPress={()=> this.props.navigation.navigate('Dashboard')} title='GO TO Dashboard'>
							</Button>
						</View>
						<Slider/>
						<Weather/>
						<Jokes/>
					</View>
				</ScrollView>
			
		);
	}
}

const styles = StyleSheet.create({
  header : {
   display: 'flex',
   flexDirection: 'row',
   padding:5,
   // justifyContent:'center',
   alignItems:'center'
  },
  title : {
  	fontWeight :'bold',
  },
  container : {
  	backgroundColor :'#fff',
  },
  stack :{
  	display:'flex',
  	flexDirection:'row',
  },
  tombol : {
  	backgroundColor : '#222',
  }
});