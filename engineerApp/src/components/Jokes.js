import axios from 'axios';
import React from 'react';
import {View, Text, StyleSheet} from 'react-native';


export default class Jokes extends React.Component{
    constructor(props){
        super(props);
        this.state={
            listJokes:[],
            selected_jokes:{}
        }
    }
    
    componentDidMount(){
        this.getData();
    }

    getData(){
        axios.get('http://localhost:5000/jokes')
        .then(res=>{
            const jokes_data = res.data;
            this.setState({listJokes: jokes_data})
            this.interval = setInterval(()=>{
                const randomData = Math.floor(Math.random()*this.state.listJokes.length);
                this.setState({selected_jokes: this.state.listJokes[randomData]})
            }, 5000)
        }).catch(err =>{
            console.log("Error Get Jokes!!!")
        })
    }

    componentWillUnmount(){
        this.interval && clearInterval(this.interval)
    }
  
    render(){
        return(
            <View style={styles.jokesitem}>
              <Text>{this.state.selected_jokes.jokes}</Text>
            </View>
        );
    }
}


const styles = StyleSheet.create({
    jokesitem:{
        width:365,
        height: 100,
        fontSize: 26,
        fontWeight: 'bold',
        borderRadius: 15,
        padding: 20,
        margin: 20,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#d3d3d3',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 10,
        },
        shadowOpacity: 0.58,
        shadowRadius: 12.00,
        elevation: 24,
    }
})