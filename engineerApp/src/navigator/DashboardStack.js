import {createStackNavigator} from '@react-navigation/stack';
import React from 'react';
import Dashboard from '../screens/Dashboard';
import OtherPage from '../screens/OtherPage';

const Stack = createStackNavigator();

export default function DashboardStack() {
	return (
		<Stack.Navigator>
			<Stack.Screen name='Dashboard' component={Dashboard} />
			<Stack.Screen name='OtherPage' component={OtherPage} />
		</Stack.Navigator>	

		)
}